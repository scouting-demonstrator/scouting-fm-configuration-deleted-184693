#!/bin/env python3

import click
import json

@click.command()
@click.argument("xml_template_file", type=click.Path(exists=True))
@click.option("--fragment_dir", default="config_fragments", help="Directory containing the fragment files to integrate into the template")
def main(xml_template_file, fragment_dir):
    """ This script pastes the contents of the config fragments referenced into the xml_template_file. If the config fragment file name doesn't end with the '.noescape' suffix, special characters will be escaped such that the output can be stored in a JSON string."""
    if "_template" not in xml_template_file:
        print(f"ERROR: xml_template_file should contain the string '_template'!")
        exit(1)
    target_file = xml_template_file.replace("_template", "")
    print(f"Reading from {xml_template_file} and writing to {target_file}..")
    with open(xml_template_file, 'r') as xml_template:
        with open(target_file, 'w') as target:
            for l in xml_template:
                if '$(' in l:
                    start = l.find('$(')
                    end = l.find(')$')
                    fragment_string = l[start:end+2]
                    fragment_file = l[start+2:end]
                    print(f"Replacing {fragment_string} with the contents of {fragment_file}.")
                    with open(fragment_dir+"/"+fragment_file, 'r') as fragment:
                        replacement_content = fragment.read()
                        if not fragment_file.endswith(".noescape"):
                            replacement_content = json.dumps(replacement_content) # Need to escape special characters when storing in JSON string
                        line_to_write = l.replace(fragment_string, replacement_content)
                else:
                    line_to_write = l

                target.write(line_to_write)

if __name__ == "__main__":
    main()
